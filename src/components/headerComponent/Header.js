import React from "react";


const Header = (props) => {
  const { title } = props;

  return (
    <div className="Header mb-5 "  >

      <div className="Header-body">
        <h1 className="Header-title">{title}</h1>

      </div>
    </div>
  )
}

export default Header;
