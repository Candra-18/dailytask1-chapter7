import React from "react";
import Header from "./headerComponent/Header";
import Button from "./buttonComponent/Button";

const Card = (props) => {
  const { title, description, imgSrc, imgAlt, btnText } = props;

  return (
   <div className="row justify-content-center">
      <div className="card me-2 mb-2" style={{ width: "18rem" }}>
      <Header title="Card 1" />
      <img src={imgSrc} className="card-img-top" alt={imgAlt} />
      <div className="card-body">
        <h5 className="card-title">{title}</h5>
        <p className="card-text">{description}</p>
        <Button variant="primary">{btnText}</Button>
      </div>
    </div>
    <div className="card me-3 mb-3" style={{ width: "18rem" }}>
      <Header title="Card 1" />
      <img src={imgSrc} className="card-img-top" alt={imgAlt} />
      <div className="card-body">
        <h5 className="card-title">{title}</h5>
        <p className="card-text">{description}</p>
        <Button variant="warning">{btnText}</Button>
      </div>
    </div>
    <div className="card me-3 mb-3" style={{ width: "18rem" }}>
      <Header title="Card 1" />
      <img src={imgSrc} className="card-img-top" alt={imgAlt} />
      <div className="card-body">
        <h5 className="card-title">{title}</h5>
        <p className="card-text">{description}</p>
        <Button variant="secondary">{btnText}</Button>
      </div>
    </div>
    <div className="card me-3 mb-3" style={{ width: "18rem" }}>
      <Header title="Card 1" />
      <img src={imgSrc} className="card-img-top" alt={imgAlt} />
      <div className="card-body">
        <h5 className="card-title">{title}</h5>
        <p className="card-text">{description}</p>
        <Button variant="black">{btnText}</Button>
      </div>
    </div>
    <div className="card me-3 mb-3" style={{ width: "18rem" }}>
      <Header title="Card 1" />
      <img src={imgSrc} className="card-img-top" alt={imgAlt} />
      <div className="card-body">
        <h5 className="card-title">{title}</h5>
        <p className="card-text">{description}</p>
        <Button variant="orange">{btnText}</Button>
      </div>
    </div>
    
   </div>
  );
};

export default Card;
