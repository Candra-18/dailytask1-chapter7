import React from "react";


const Heading = (props) => {
  const { title } = props;

  return (
    <div className="Heading mb-3 "  >

      <div className="Heading-body">
        <h1 className="Heading-title">{title}</h1>

      </div>
    </div>
  )
}

export default Heading;
