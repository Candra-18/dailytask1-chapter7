import Card from './components/Card'
import Header from './components/headerComponent/Header'
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import Heading from './components/headingComponent/Heading';

function App() {
  return (
    <div className="App">
      <Heading 
        title="David  Disini"
      />

      <Header
        title= "Belajar React Js" 
         />
      
      <Card
        title="Hello"
        description="Lorem ipsum dolor sit amet"
        btnText="Go Somewhere"
        btnHref="https://google.com"
        imgSrc="https://placeimg.com/320/240/any"
        imgAlt="Hello"
      />
    </div>
  );
}

export default App;
